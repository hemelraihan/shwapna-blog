<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Module;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $moduleappDashboard = Module::updateOrCreate([
            'name' => 'Admin Dashboard'
        ]);

        Permission::updateOrCreate([
            'module_id' => $moduleappDashboard->id,
            'name' => 'Access Dashboard',
            'slug' => 'admin.dashboard'
        ]);



        $moduleappRole = Module::updateOrCreate([
            'name' => 'Role Management'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappRole->id,
            'name' => 'Access Role',
            'slug' => 'admin.roles.index'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappRole->id,
            'name' => 'Create Role',
            'slug' => 'admin.roles.create'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappRole->id,
            'name' => 'Edit Role',
            'slug' => 'admin.roles.edit'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappRole->id,
            'name' => 'Delete Role',
            'slug' => 'admin.roles.destroy'
        ]);

        $moduleappUser = Module::updateOrCreate([
            'name' => 'User Management'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappUser->id,
            'name' => 'Access User',
            'slug' => 'admin.users.index'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappUser->id,
            'name' => 'Create User',
            'slug' => 'admin.users.create'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappUser->id,
            'name' => 'Edit User',
            'slug' => 'admin.users.edit'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappUser->id,
            'name' => 'Delete User',
            'slug' => 'admin.users.destroy'
        ]);

        $moduleappPost = Module::updateOrCreate([
            'name' => 'Post Management'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappPost->id,
            'name' => 'Access Post',
            'slug' => 'author.posts.index'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappPost->id,
            'name' => 'Create Post',
            'slug' => 'author.posts.create'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappPost->id,
            'name' => 'Edit Post',
            'slug' => 'author.posts.edit'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappPost->id,
            'name' => 'Delete Post',
            'slug' => 'author.posts.destroy'
        ]);

        $moduleappPendingPost = Module::updateOrCreate([
            'name' => 'Pending Post'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappPendingPost->id,
            'name' => 'Access Pending Post',
            'slug' => 'author.pendings.index'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappPendingPost->id,
            'name' => 'Approve Post',
            'slug' => 'author.pendings.approve'
        ]);

        $moduleappTag = Module::updateOrCreate([
            'name' => 'Tag Management'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappTag->id,
            'name' => 'Access Tag',
            'slug' => 'author.tags.index'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappTag->id,
            'name' => 'Create Tag',
            'slug' => 'author.tags.create'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappTag->id,
            'name' => 'Edit Tag',
            'slug' => 'author.tags.edit'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappTag->id,
            'name' => 'Delete Tag',
            'slug' => 'author.tags.destroy'
        ]);

        $moduleappPost = Module::updateOrCreate([
            'name' => 'Category Management'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappPost->id,
            'name' => 'Access Category',
            'slug' => 'author.categories.index'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappPost->id,
            'name' => 'Create Category',
            'slug' => 'author.categories.create'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappPost->id,
            'name' => 'Edit Category',
            'slug' => 'author.categories.edit'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappPost->id,
            'name' => 'Delete Category',
            'slug' => 'author.categories.destroy'
        ]);

        $moduleappPost = Module::updateOrCreate([
            'name' => 'Subscribers'
        ]);
        Permission::updateOrCreate([
            'module_id' => $moduleappPost->id,
            'name' => 'Access Subscriber',
            'slug' => 'author.subscribers.index'
        ]);

    }
}
