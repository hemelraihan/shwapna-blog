<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::updateOrCreate([
            'role_id' => Role::where('slug','admin')->first()->id,
            'name' => 'Admin',
            'email' => 'hemel.1997@gmail.com',
            'password' => Hash::make('password'),
            'status' => true
        ]);

        Admin::updateOrCreate([
            'role_id' => Role::where('slug','author')->first()->id,
            'name' => 'Author',
            'email' => 'raihanhemel.aiub@gmail.com',
            'password' => Hash::make('password'),
            'status' => true
        ]);
    }
}
