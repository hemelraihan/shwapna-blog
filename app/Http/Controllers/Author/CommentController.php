<?php

namespace App\Http\Controllers\Author;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Comment;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function index()
    {
        $posts = Auth::user()->posts;
        return view('author.comments',compact('posts'));
    }

    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        if($comment->post->user->id == Auth::id())
        {
            $comment->delete();
            notify()->success("Comment Successfully Delete","Updated");
        }
        else
        {
            notify()->Error("You have not access to delete this","Error");
        }

        return back();
    }
}
