<?php

namespace App\Http\Controllers\Author;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Models\Category;
use App\Models\Tag;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NewAuthorPost;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Auth::User()->posts()->latest()->get();
        return view('author.post.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('author.post.create',compact('categories','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'image' => 'required',
            'categories' => 'required',
            'tags' => 'required',
            'body' => 'required',
        ]);
        $image = $request->file('image');
        $slug = Str::slug($request->title);
        if(isset($image))
        {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('postphoto'))
            {
                Storage::disk('public')->makeDirectory('postphoto');
            }
            $postImage = Image::make($image)->resize(1600,1066)->save(90);
            Storage::disk('public')->put('postphoto/'.$imageName,$postImage);

        }
        else
        {
            $imageName = "default.png";
        }
        $post = new Post();
        $post->user_id = Auth::id();
        $post->title = $request->title;
        $post->slug = $slug;
        $post->image = $imageName;
        $post->body = $request->body;
        if(isset($request->status))
        {
            $post->status = true;
        }
        else
        {
            $post->status = false;
        }
        $post->is_approved = false;
        $post->save();

        //for many to many
        $post->categories()->attach($request->categories);
        $post->tags()->attach($request->tags);

        $users = User::where('role_id','1')->get();
        Notification::send($users,new NewAuthorPost($post));

        notify()->success("Post Successfully Created","Created");
        return redirect()->route('author.posts.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        if($post->user_id!=Auth::id())
        {
            notify()->success("You are not suppose to access here!!");
            return redirect()->back();
        }
        else
        {
            return view('author.post.show',compact('post'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        if($post->user_id!=Auth::id())
        {
            notify()->success("You are not suppose to access here!!");
            return redirect()->back();
        }
        else
        {
            $categories = Category::all();
            $tags = Tag::all();
            return view('author.post.edit',compact('post','categories','tags'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        if($post->user_id!=Auth::id())
        {
            notify()->success("You are not suppose to access here!!");
            return redirect()->back();
        }
        $this->validate($request,[
            'title' => 'required',
            'image' => 'image',
            'categories' => 'required',
            'tags' => 'required',
            'body' => 'required',
        ]);
        $image = $request->file('image');
        $slug = Str::slug($request->title);
        if(isset($image))
        {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('postphoto'))
            {
                Storage::disk('public')->makeDirectory('postphoto');
            }

            //delete old posting image

            if(Storage::disk('public')->exists('postphoto/'.$post->image))
            {
                Storage::disk('public')->delete('postphoto/'.$post->image);
            }

            $postImage = Image::make($image)->resize(1600,1066)->save(90);
            Storage::disk('public')->put('postphoto/'.$imageName,$postImage);

        }
        else
        {
            $imageName = $post->image;
        }

        $post->user_id = Auth::id();
        $post->title = $request->title;
        $post->slug = $slug;
        $post->image = $imageName;
        $post->body = $request->body;
        if(isset($request->status))
        {
            $post->status = true;
        }
        else
        {
            $post->status = false;
        }
        $post->is_approved = false;
        $post->save();

        //for many to many
        $post->categories()->sync($request->categories);
        $post->tags()->sync($request->tags);

        notify()->success("Post Successfully Updated");
        return redirect()->route('author.posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if($post->user_id!=Auth::id())
        {
            notify()->success("You are not suppose to access here!!");
            return redirect()->back();
        }

        if(Storage::disk('public')->exists('postphoto/'.$post->image))
        {
            Storage::disk('public')->delete('postphoto/'.$post->image);

        }
        $post->categories()->detach();
        $post->tags()->detach();
        $post->delete();
        notify()->success("Post Successfully Deleted");
        return redirect()->back();
    }
}
