<?php

namespace App\Http\Controllers\Adminlogin;

use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\storage;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {


        $this->middleware('guest:admin')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('adminlogin.login');
    }

    /*public function login(Request $request)
    {
        $inputVal = $request->all();

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if(auth()->attempt(array('email' => $inputVal['email'], 'password' => $inputVal['password']))){
            if (auth()->admin()->role->id == 2) {
                return redirect()->route('admin.dashboard');
            }else{
                return redirect()->route('author.dashboard');
            }
        }else{
            return redirect()->route('admin.login')
                ->with('error','Email & Password are incorrect.');
        }
    }*/

    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProvider($provider)
    {
        $user = Socialite::driver($provider)->user();
        $existingUser = User::whereEmail($user->getEmail())->first();
        if($existingUser)
        {
            Auth::login($existingUser);
        }
        else
        {

            //upload image from github
            $image = $user->getAvatar();
            $name = $user->getNickname();
            if($user->getAvatar())
            {
                $currentDate = Carbon::now()->toDateString();
                $imagename = $name.'-'.$currentDate.'-'.uniqid();

                if(!Storage::disk('public')->exists('userphoto'))
                {
                    Storage::disk('public')->makeDirectory('userphoto');
                }
                $userimg = Image::make($image)->resize(1600,1600)->save($imagename,90);

                Storage::disk('public')->put('userphoto/'.$imagename,$userimg);
            }
            else
            {
                $imagename = 'default.png';
            }

            $newUser = User::create([
                'role_id' => Role::where('slug','author')->first()->id,
                'name' => $user->getNickname(),
                'email' => $user->getEmail(),
                'status' => true,
                'image' => $imagename,
                ]);

            Auth::login($newUser);
        }
        return redirect($this->redirectPath());
    }


}
