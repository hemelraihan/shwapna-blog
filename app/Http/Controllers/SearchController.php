<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $query = $request->input('query');
        $posts = Post::where('title','LIKE',"%$query%")->approved()->get();
        $recentposts = Post::orderBy('id','desc')->approved()->take(4)->get();
        $allpost = Post::all();
        return view('search',compact('posts','query','recentposts','allpost'));
    }
}
