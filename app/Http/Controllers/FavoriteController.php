<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{
    public function add($post)
    {
        $user = Auth::user();
        $admin = Auth::guard('admin')->user();
        $isFavorite = $user->favorite_posts->where('post_id',$post)->count();
        $isFavoritepost = $admin->favoritePosts->where('post_id',$post)->count();
        //return $isFavorite;
       if($isFavorite == 0)
        {
            $user->favorite_posts()->attach($post);
            $admin->favoritePosts()->attach($post);
            notify()->success("Post Successfully added to favorite list","Added");
            return back();
        }
        else
        {
            $user->favorite_posts()->detach($post);
            $admin->favoritePosts()->detach($post);
            notify()->success("Post Successfully removed from favorite list","removed");
            return back();
        }
    }
}
