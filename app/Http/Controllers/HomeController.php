<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //$idd= Auth::guard('admin');

        $categories = Category::all();
        $posts = Post::latest()->paginate(6);
        return view('homepage',compact('categories','posts'));
    }

    public function showpost($id)
    {
        $categories = Category::all();
        $recentposts = Post::orderBy('id','asc')->approved()->take(4)->get();
        $posts = Category::find($id)->posts()->approved()->get();
        return view('viewallposts',compact('posts','recentposts','categories'));
    }

    public function postdetails($id,Request $request)
    {
        $recentposts = Post::orderBy('id','desc')->approved()->take(4)->get();
        $postdetails = Post::all();
        $posts = Post::where('id',$id)->get();
        Post::find($id)->increment('view_count');
        $categories = Post::find($id)->categories()->get();
        $post = Post::latest();
       return view('postdetails',compact('posts','post','recentposts','postdetails','categories'));
    }

    public function test()
    {
        return view('test');
    }
}
