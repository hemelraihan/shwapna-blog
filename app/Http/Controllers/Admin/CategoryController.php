<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Gate;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('author.categories.index');
        $categories = Category::latest()->get();
        return view('admin.category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('author.categories.create');
        return view('admin.category.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('author.categories.create');
        $this->validate($request,[
            'name' => 'required|unique:categories',
            'image' => 'required|mimes:jpeg,,bmp,png,jpg',
        ]);
        //get form image
        $image = $request->file('image');
        $slug = Str::slug($request->name);

        if(isset($image))
        {
            $currentDate = Carbon::now()->toDateString();
            $imagename = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();

            //check image folder is axist
            if(!Storage::disk('public')->exists('categoryphoto'))
            {
                Storage::disk('public')->makeDirectory('categoryphoto');
            }
            //resize image
            $category = Image::make($image)->resize(500,333)->save($imagename,90);
            Storage::disk('public')->put('categoryphoto/'.$imagename,$category);

             //check image folder is axist
            /* if(!Storage::disk('public')->exists('category/slider'))
             {
                 Storage::disk('public')->makeDirectory('category/slider');
             }
              //resize image
            $slider = Image::make($image)->resize(500,333)->save($imagename,90);
            Storage::disk('public')->put('category/slider/'.$imagename,$slider);*/

        }

        $category = Category::create([
            'name' => $request->name,
            'slug' => $slug,
            'image' => $imagename,

        ]);
        notify()->success("Category Addeed",'success');
        return redirect()->route('admin.categories.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        Gate::authorize('author.categories.edit');
         return view('admin.category.form',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        Gate::authorize('author.categories.edit');
        $this->validate($request,[
            'name' => 'required',
            'image' => 'mimes:jpeg,,bmp,png,jpg',
        ]);
        //get form image
        $image = $request->file('image');
        $slug = Str::slug($request->name);



        if(isset($image))
        {
            $currentDate = Carbon::now()->toDateString();
            $imagename = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();

            //check image folder is axist
            if(!Storage::disk('public')->exists('categoryphoto'))
            {
                Storage::disk('public')->makeDirectory('categoryphoto');
            }

            //Delete old image
            if(Storage::disk('public')->exists('categoryphoto/'.$category->image))
            {
                Storage::disk('public')->delete('categoryphoto/'.$category->image);
            }

            //resize image
            $categoryimg = Image::make($image)->resize(1600,479)->save($imagename,90);
            Storage::disk('public')->put('categoryphoto/'.$imagename,$categoryimg);

             //check image folder is axist
            /* if(!Storage::disk('public')->exists('category/slider'))
             {
                 Storage::disk('public')->makeDirectory('category/slider');
             }

             //Delete old image
            if(Storage::disk('public')->exists('category/slider/'.$category->image))
            {
                Storage::disk('public')->delete('category/slider/'.$category->image);
            }
              //resize image
            $slider = Image::make($image)->resize(500,333)->save($imagename,90);
            Storage::disk('public')->put('category/slider/'.$imagename,$slider);*/

        }
        else
        {
            $imagename = $category->image;
        }

        $category->update([
            'name' => $request->name,
            'slug' => $slug,
            'image' => $imagename,

        ]);

        notify()->success("Category Updated",'success');
        return redirect()->route('admin.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        Gate::authorize('author.categories.destroy');
        $category->delete();
        notify()->success("Category Deleted Successfully",'success');
        return back();
    }
}
