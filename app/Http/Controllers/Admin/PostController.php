<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Admin;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;
use App\Notifications\AuthorPostApproved;
use App\Notifications\NewPostNotification;
use App\Notifications\NewAuthorPost;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Gate;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('author.posts.index');
        //$posts = Auth::User()->posts()->latest()->get();
        $posts = Auth::guard('admin')->user()->posts()->latest()->get();
        //$posts = Post::latest()->get();
        return view('admin.post.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('author.posts.create');
        $post = Post::all();
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.post.create',compact('categories','tags','post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('author.posts.create');
        $this->validate($request,[
            'title' => 'required',
            'image' => 'required',
            'categories' => 'required',
            'tags' => 'required',
            'body' => 'required',
        ]);
        $image = $request->file('image');
        $slug = Str::slug($request->title);
        if(isset($image))
        {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('postphoto'))
            {
                Storage::disk('public')->makeDirectory('postphoto');
            }
            $postImage = Image::make($image)->resize(900,600)->save(90);
            Storage::disk('public')->put('postphoto/'.$imageName,$postImage);

        }
        else
        {
            $imageName = "default.png";
        }

        $post = new Post();
        $post->admin_id = Auth::id();
        $post->title = $request->title;
        $post->slug = $slug;
        $post->image = $imageName;
        $post->body = $request->body;
        if(isset($request->status))
        {
            $post->status = true;
        }
        else
        {
            $post->status = false;
        }
        if(Auth::guard('admin')->user()->role_id == 2)
        {
            $post->is_approved = false;
        }
        else
        {
            $post->is_approved = true;
        }

        $post->save();


        //for many to many
        $post->categories()->attach($request->categories);
        $post->tags()->attach($request->tags);

        if(Auth::guard('admin')->user()->role_id == 2)
        {
            $admins = Admin::where('role_id','1')->get();
            Notification::send($admins,new NewAuthorPost($post));
        }


        $subscribers = Subscriber::all();
        foreach($subscribers as $subscriber)
        {
            Notification::route('mail',$subscriber->email)
                        ->notify(new NewPostNotification($post,$subscriber));
        }

        notify()->success("Post Successfully Created","Added");
        return redirect()->route('admin.posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        Gate::authorize('author.posts.create');
        return view('admin.post.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        Gate::authorize('author.posts.edit');
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.post.edit',compact('post','categories','tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        Gate::authorize('author.posts.edit');
        $this->validate($request,[
            'title' => 'required',
            'image' => 'image',
            'categories' => 'required',
            'tags' => 'required',
            'body' => 'required',
        ]);
        $image = $request->file('image');
        $slug = Str::slug($request->title);
        if(isset($image))
        {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('postphoto'))
            {
                Storage::disk('public')->makeDirectory('postphoto');
            }

            //delete old posting image

            if(Storage::disk('public')->exists('postphoto/'.$post->image))
            {
                Storage::disk('public')->delete('postphoto/'.$post->image);
            }

            $postImage = Image::make($image)->resize(1600,1066)->save(90);
            Storage::disk('public')->put('postphoto/'.$imageName,$postImage);

        }
        else
        {
            $imageName = $post->image;
        }

        $post->user_id = Auth::id();
        $post->title = $request->title;
        $post->slug = $slug;
        $post->image = $imageName;
        $post->body = $request->body;
        if(isset($request->status))
        {
            $post->status = true;
        }
        else
        {
            $post->status = false;
        }
        $post->is_approved = true;
        $post->save();

        //for many to many
        $post->categories()->sync($request->categories);
        $post->tags()->sync($request->tags);

        notify()->success("Post Successfully Updated","Updated");
        return redirect()->route('admin.posts.index');
    }

    public function pending()
    {
        Gate::authorize('author.pendings.index');
        $posts = Post::where('is_approved',false)->get();
        return view('admin.post.pending',compact('posts'));
    }

    public function approval($id)
    {
        Gate::authorize('author.pendings.approve');
        $post = Post::find($id);
        if(Auth::guard('admin')->user()->role_id == 1)
        {
            if($post->is_approved == false)
            {
                $post->is_approved = true;
                $post->save();
                if(Auth::guard('admin')->user()->role_id == 1)
                {
                    $post->admin->notify(new AuthorPostApproved($post));
                }
                else
                {
                    $admins = Admin::where('role_id','1')->get();
                    Notification::send($admins,new NewAuthorPost($post));
                }


                $subscribers = Subscriber::all();
                foreach($subscribers as $subscriber)
                {
                    Notification::route('mail',$subscriber->email)
                                ->notify(new NewPostNotification($post,$subscriber));
                }

                notify()->success("Post Successfully Approved","Approved");
            }
            else
            {
                notify()->success("Post Already Aproved");
            }
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        Gate::authorize('author.posts.destroy');
        if(Storage::disk('public')->exists('postphoto/'.$post->image))
        {
            Storage::disk('public')->delete('postphoto/'.$post->image);

        }
        $post->categories()->detach();
        $post->tags()->detach();
        $post->delete();
        notify()->success("Post Successfully Deleted","Deleted");
        return redirect()->back();
    }
}
