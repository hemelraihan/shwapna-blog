<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Comment;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function index()
    {
        $posts = Auth::guard('admin')->user()->posts;
        //$comments = Comment::latest()->get();
        return view('admin.comments',compact('posts'));
    }

    public function destroy($id)
    {
        Comment::findOrFail($id)->delete();
        notify()->success("Comment Successfully Delete","Updated");
        return back();
    }
}
