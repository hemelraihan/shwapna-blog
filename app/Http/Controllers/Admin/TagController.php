<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Gate;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('author.tags.index');
        $tags = Tag::latest()->get();
        return view('admin.tags.index',compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('author.tags.create');
        return view('admin.tags.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('author.tags.create');
        $this->validate($request,[
            'tagname' => 'required'
        ]);

        $tag = Tag::create([
            'name' => $request->tagname,
            'slug' => Str::slug($request->tagname),

        ]);

        notify()->success("Tag Addeed",'success');
        return redirect()->route('admin.tags.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        Gate::authorize('author.tags.edit');
         return view('admin.tags.form',compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        Gate::authorize('author.tags.edit');
        $this->validate($request,[
            'tagname' => 'required'
        ]);
        $tag->update([
            'name' => $request->tagname,
            'slug' => Str::slug($request->tagname),

        ]);
        notify()->success("Tag Updated",'success');
        return redirect()->route('admin.tags.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        Gate::authorize('author.tags.destroy');
        $tag->delete();
        notify()->success("Tag Deleted Successfully",'success');
        return back();
    }
}
