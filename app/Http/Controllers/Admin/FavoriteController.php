<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;

class FavoriteController extends Controller
{
    public function index()
    {
        //$posts = Auth::guard('admin')->user()->favoritePosts;
        //dd($post);
        //$posts = Post::all();
        $posts = Auth::guard('admin')->user()->posts;
        return view('admin.favorite',compact('posts'));
    }
}
