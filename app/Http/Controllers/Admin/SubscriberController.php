<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subscriber;
use Illuminate\Support\Facades\Gate;

class SubscriberController extends Controller
{
    public function index()
    {
        Gate::authorize('author.subscribers.index');
        $subscribers = Subscriber::latest()->get();
        return view('admin.subscriber',compact('subscribers'));
    }

    public function destroy($subscriber)
    {
        $subscriber = Subscriber::findOrFail($subscriber)->delete();
        notify()->success("Email Successfully Deleted","Deleted");
        return redirect()->back();
    }
}
