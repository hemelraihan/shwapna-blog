<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');
Route::get('/posts/{id}', 'HomeController@showpost')->name('home.post');
Route::get('/postdetails/{id}', 'HomeController@postdetails')->name('home.postdetails');
Route::get('/test', 'HomeController@test')->name('home.test');

Route::get('/search', 'SearchController@search')->name('search');

Auth::routes();

//for admin authentication
//Route::get('admin/home', 'AdminLoginController@index');
Route::get('adminlogin', 'Adminlogin\LoginController@showLoginForm')->name('admin.login');
Route::post('adminlogin', 'AdminLogin\LoginController@login');
Route::post('admin-password/email', 'AdminLogin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('admin-password/reset', 'AdminLogin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('admin-password/reset', 'AdminLogin\ResetPasswordController@reset')->name('admin.password.reset');
Route::get('admin-password/reset/{token}', 'AdminLogin\ResetPasswordController@showResetForm')->name('admin.password.reset');






Route::group(['middleware'=>['auth']],function(){
    Route::post('favorite/{post}/add','FavoriteController@add')->name('post.favorite');
    Route::post('comment/{post}','CommentController@store')->name('comment.store');

});


Route::group(['as'=>'admin.','prefix'=>'admin', 'namespace'=>'Admin', 'middleware'=>['auth:admin',]], function(){
    Route::get('dashboard','DashboardController@index')->name('dashboard');
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::resource('tags','TagController');
    Route::resource('categories','CategoryController');
    Route::resource('posts','PostController');

    Route::get('/pending/post','PostController@pending')->name('post.pending');
    Route::put('/post/{id}/approve','PostController@approval')->name('post.approve');

    Route::get('/subscriber','SubscriberController@index')->name('subscriber.index');
    Route::delete('/subscriber/{subscriber}','SubscriberController@destroy')->name('subscriber.destroy');

    Route::get('/favorite','FavoriteController@index')->name('favorite.index');

    Route::get('/comments','CommentController@index')->name('comment.index');
    Route::delete('/comments/{id}','CommentController@destroy')->name('comment.destroy');

});

Route::group(['as'=>'author.','prefix'=>'author','namespace'=>'Author','middleware'=>['auth']],function(){
    Route::get('dashboard','DashboardController@index')->name('dashboard');
    Route::resource('users','UserController');
    Route::resource('posts','PostController');

    Route::get('/favorite','FavoriteController@index')->name('favorite.index');

    Route::get('/comments','CommentController@index')->name('comment.index');
    Route::delete('/comments/{id}','CommentController@destroy')->name('comment.destroy');

});

/*Route::group(['as'=>'user.','prefix'=>'user','namespace'=>'User','middleware'=>['auth','user']],function(){
    Route::get('dashboard','DashboardController@index')->name('dashboard');


});*/

Route::post('subscriber','SubscriberController@store')->name('subscriber.store');
Route::get('/login/{provider}', [LoginController::class, 'redirectToProvider'])->name('login.provider');
Route::get('/login/{provider}/callback', [LoginController::class, 'handleProvider'])->name('login.callback');



