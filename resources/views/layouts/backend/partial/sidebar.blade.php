<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="{{ Storage::disk('public')->url('userphoto/'.Auth::user()->image) }}" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</div>
            <div class="email">{{ Auth::user()->email }}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                    <li role="separator" class="divider"></li>
                    <li>


                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                               <i class="material-icons">input</i>Sign Out
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>


                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>

            @if(Request::is('admin*'))
            <li class="{{ Request::is('admin/dashboard') ? 'active' : '' }}">
                <a href="{{route('admin.dashboard')}}">
                    <i class="material-icons">dashboard</i>
                    <span>Dashboard</span>
                </a>
                </li>

                @if(Auth::user()->hasPermission('admin.roles.index'))
                <li class="{{ Request::is('admin/roles*') ? 'active' : '' }}">
                <a href="{{route('admin.roles.index')}}">
                    <i class="material-icons">label</i>
                    <span>Roles Management</span>
                </a>
                </li>
                @endif

                @if(Auth::user()->hasPermission('admin.users.index'))
                <li class="{{ Request::is('admin/users*') ? 'active' : '' }}">
                <a href="{{route('admin.users.index')}}">
                    <i class="material-icons">label</i>
                    <span>Users</span>
                </a>
                </li>
                @endif
                @if(Auth::user()->hasPermission('author.tags.index'))
                <li class="{{ Request::is('admin/tags*') ? 'active' : '' }}">
                    <a href="{{route('admin.tags.index')}}">
                        <i class="material-icons">label</i>
                        <span>Tag</span>
                    </a>
                </li>
                @endif

                @if(Auth::user()->hasPermission('author.categories.index'))
                <li class="{{ Request::is('admin/categories*') ? 'active' : '' }}">
                    <a href="{{route('admin.categories.index')}}">
                        <i class="material-icons">label</i>
                        <span>Category</span>
                    </a>
                </li>
                @endif

                @if(Auth::user()->hasPermission('author.posts.index'))
                <li class="{{ Request::is('admin/posts*') ? 'active' : '' }}">
                    <a href="{{route('admin.posts.index')}}">
                        <i class="material-icons">label</i>
                        <span>Post</span>
                    </a>
                </li>
                @endif

                @if(Auth::user()->hasPermission('author.pendings.index'))
                <li class="{{ Request::is('admin/pending/post') ? 'active' : '' }}">
                    <a href="{{route('admin.post.pending')}}">
                        <i class="material-icons">library_books</i>
                        <span>Pending Post</span>
                    </a>
                </li>
                @endif

                    <li class="{{ Request::is('admin/favorite') ? 'active' : '' }}">
                        <a href="{{route('admin.favorite.index')}}">
                            <i class="material-icons">favorite</i>
                            <span>Favorite Posts</span>
                        </a>
                    </li>

                    <li class="{{ Request::is('admin/comments') ? 'active' : '' }}">
                        <a href="{{route('admin.comment.index')}}">
                            <i class="material-icons">comment</i>
                            <span>Comments</span>
                        </a>
                    </li>

                    @if(Auth::user()->hasPermission('author.subscribers.index'))
                    <li class="{{ Request::is('admin/subscriber') ? 'active' : '' }}">
                    <a href="{{route('admin.subscriber.index')}}">
                        <i class="material-icons">subscriptions</i>
                        <span>Subscribers</span>
                    </a>
                    </li>
                    @endif

                <li class="header">System</li>
                <li class="">
                <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                               <i class="material-icons">input</i>
                               <span>Log Out</span>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                </li>
            @endif

            @if(Request::is('author*'))

            <li class="{{ Request::is('author/dashboard') ? 'active' : '' }}">
                <a href="{{route('author.dashboard')}}">
                    <i class="material-icons">dashboard</i>
                    <span>Dashboard</span>
                </a>
                </li>

               
                <li class="header">System</li>

                <li class="">
                <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                               <i class="material-icons">input</i>
                               <span>Log Out</span>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                </li>

            @endif



        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; 2016 - 2017 <a href="javascript:void(0);">adminBSB - Material Design</a>.
        </div>
        <div class="version">
            <b>Version: </b> 1.0.5
        </div>
    </div>
    <!-- #Footer -->
</aside>
