@extends('layouts.backend.app')

@section('title','Users')

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@push('css')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    .dropify-wrapper .dropify-message p {
        font-size: initial;
    }
</style>
@endpush



@section('content')


<div class="container-fluid">

    <!-- Vertical Layout | With Floating Label -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <div>{{ isset($role) ? 'Edit ' : 'Create '}}Users</div>
                    </h2>

                </div>
                <div class="body">
                    <form enctype="multipart/form-data" method="POST" action="{{isset($user) ? route('admin.users.update',$user->id) : route('admin.users.store')}}">
                        @csrf
                        @isset($user)
                        @method('PUT')
                        @endisset
                        <div class="card-body">
                        <h5 class="card-title">Manage Users</h5>
                        <div class="form-group">
                        <label for="name">User Name</label>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name ?? old('name') }}"  autofocus>

                                   @error('name')
                                       <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                       </span>
                                   @enderror

                          </div>

                          <div class="form-group">
                        <label for="name">Email</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email ?? old('email') }}"  autofocus>

                                   @error('email')
                                       <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                       </span>
                                   @enderror

                          </div>

                          <div class="form-group">
                        <label for="name">Password</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password">

                                   @error('password')
                                       <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                       </span>
                                   @enderror

                          </div>

                          <div class="form-group">
                        <label for="confirm_password">Confirm Password</label>
                        <input id="confirm_password" type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation">

                                   @error('password')
                                       <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                       </span>
                                   @enderror

                          </div>




                                        <div class="card-body">
                                        <h5 class="card-title">Select Role and Status</h5>
                                        <div class="form-group">
                                            <label for="role">Select Role</label>
                                            <select id="role"  class="  @error('role') is-invalid @enderror" name="role_id">
                                                @foreach ($roles as $role )
                                                    <option value="{{ $role->id }}" @isset($user) {{$user->role->id == $role->id ? 'selected' : ''}} @endisset>{{$role->name}}</option>
                                                @endforeach
                                            </select>

                                                       @error('role')
                                                           <span class="invalid-feedback" user="alert">
                                                               <strong>{{ $message }}</strong>
                                                           </span>
                                                       @enderror
                                                </div>

                                                <div class="form-group">
                                                    <label for="avatar">Avatar</label>
                                                    <input id="avatar" type="file" class=" dropify form-control @error('avatar') is-invalid @enderror" data-default-file="{{ isset($user) ? Storage::disk('public')->url('userphoto/'.$user->image) : '' }}" name="avatar">



                                                               @error('avatar')
                                                                   <span class="invalid-feedback" user="alert">
                                                                       <strong>{{ $message }}</strong>
                                                                   </span>
                                                               @enderror
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="custom-control custom-switch">
                                                                <input class="custom-control-input" type="checkbox" @isset($user) {{$user->status == true ? 'checked' : ''}} @endisset id="status" name="status">
                                                                <label class="custom-control-label" for="status">Status</label>
                                                              </div>
                                                              @error('status')
                                                              <span class="text-danger" user="alert">
                                                                  <strong>{{ $message }}</strong>
                                                              </span>
                                                          @enderror
                                                        </div>


                        <a type="button" class="btn btn-danger m-t-15 waves-effect" href="{{ route('admin.users.index') }}">BACK</a>
                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">
                            @isset($user)
                            Update
                            @else
                            Create
                            @endisset
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Vertical Layout | With Floating Label -->

</div>

@endsection

@push('js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
  // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
    $('.dropify').dropify();

});
</script>
@endpush
