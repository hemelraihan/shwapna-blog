@extends('layouts.backend.app')

@section('title','Users')

@push('css')

@endpush

@section('content')

<div class="container-fluid">
    <div class="block-header">
        @if($auth->hasPermission('admin.users.create'))
      <a class="btn btn-primary waves effect" href="{{ route('admin.users.create') }}">
      <li class="material-icons">add</li>
      <span>Add New User</span>
      </a>
      @endif
    </div>

    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        All Users
                        <span class="badge bg-blue">{{$users->count()}}</span>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Joined At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                         <tbody>
                         @foreach($users as $key=>$user)
                         <tr>
                         <td>{{$key + 1}}</td>
                         <td><div class="widget-content p-8">
                                                        <div class="widget-content-wrapper">
                                                            <div class=" widget-content-left mr-3">
                                                                <div class="widget-content-left">
                                                                    <img width="40" class="rounded-circle"
                                                                    src="{{ Storage::disk('public')->url('userphoto/'.$user->image) }}" alt="User Avatar">
                                                                </div>
                                                            </div>
                                                            <div class="widget-content-left flex2">
                                                                <div class="widget-heading">{{$user->name}}</div>
                                                                <div class="widget-subheading opacity-7">
                                                                    @if($user->role)
                                                                       <span class="badge badge-info">{{$user->role->name}}</span>
                                                                    @else
                                                                       <span class="badge badge-danger">No Role Found</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                        </td>
                         <td>{{$user->email}}</td>
                         <td>
                            @if($user->status == true)
                                <span class="badge badge-info">Active</span>
                            @else
                                <span class="badge badge-danger">Inactive :(</span>
                            @endif
                         </td>
                         <td>{{$user->created_at}}</td>
                         @if($auth->hasPermission('admin.users.edit'))
                         <td class="text-center">
                         <a href="{{route('admin.users.edit',$user->id)}}" class="btn btn-info waves-effect">
                         <i class="material-icons">edit</i>
                         </a>
                         @endif

                         @if($auth->hasPermission('admin.users.destroy'))
                         <button class="btn btn-danger waves effect" type="button"
                         onclick="deleteuser({{$user->id}})">
                         <li class="material-icons">delete</li>
                         </button>
                         <form id="deleteform-{{$user->id}}" action="{{route('admin.users.destroy',$user->id)}}" method="POST" style="display: none;">
                         @csrf
                        @method('DELETE')
                        @endif
                        </form>
                         </td>
                         </tr>
                         @endforeach


                         </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
</div>

@endsection

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">
    function deleteuser(id)
    {
        Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
   event.preventDefault();
   document.getElementById('deleteform-'+id).submit();
  }
})
    }
    </script>


@push('js')

@endpush
