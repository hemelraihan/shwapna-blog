@extends('layouts.backend.app')

@section('title','Post')

@push('css')
 <!-- JQuery DataTable Css -->
 <link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="container-fluid">
            <div class="block-header">

            </div>

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                All Comments

                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Comments Info</th>
                                            <th class="text-center">Post Info</th>
                                            <th class="text-center">Actions</th>
                                        </tr>
                                    </thead>

                                 <tbody>
                                     @foreach ( $posts as $post)

                                 @foreach($post->comments as $key=>$comment)
                                 <tr>
                                 <td>
                                     <div class="media">
                                         <div class="media-left">
                                             <a href="">
                                                 <img class="media-object" src="{{Storage::disk('public')->url('userphoto/'.$comment->User->image)}}" width="64" height="64">
                                             </a>
                                         </div>
                                         <div class="media-body">
                                             <h4 class="media-heading">{{$comment->User->name}}
                                            <small>{{$comment->created_at->diffForHumans()}}</small></h4>
                                            <p>{{$comment->comment}}</p>
                                         </div>
                                     </div>
                                 </td>

                                 <td>
                                    <div class="media">
                                        <div class="media-right">
                                            <a target="_blank" href="{{route('home.postdetails',$comment->post->id)}}">
                                                <img class="media-object" src="{{Storage::disk('public')->url('postphoto/'.$comment->post->image)}}" width="64" height="64">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <a target="_blank" href="{{route('home.postdetails',$comment->post->id)}}">
                                            <h4 class="media-heading">{{Str::limit($comment->post->title,'40')}}</h4>
                                            </a>
                                           <p>by <strong>{{$comment->post->admin->name}}</strong></p>
                                        </div>
                                    </div>
                                </td>

                                 <td class="text-center">
                                    <button class="btn btn-danger waves effect" type="button"
                                    onclick="deletepost$post({{$comment->id}})">
                                    <li class="material-icons">delete</li>
                                    </button>
                                    <form id="deleteform-{{$comment->id}}" action="{{route('admin.comment.destroy',$comment->id)}}" method="POST" style="display: none;">
                                    @csrf
                                   @method('DELETE')
                                   </form>

                                 </td>
                                 </tr>
                                 @endforeach
                                 @endforeach


                                 </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
@endsection

@push('js')
 <!-- Jquery DataTable Plugin Js -->
 <script src="{{asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/tables/jquery-datatable.js')}}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script type="text/javascript">
    function deletepost$post(id)
    {
        Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
   event.preventDefault();
   document.getElementById('deleteform-'+id).submit();
  }
})
    }
    </script>



<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script type="text/javascript">
    function approvepost$post(id)
    {
        Swal.fire({
  title: 'Are you sure?',
  text: "You approve this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, approve it!'
}).then((result) => {
  if (result.isConfirmed) {
   event.preventDefault();
   document.getElementById('approvalform').submit();
  }
})
    }
    </script>

@endpush
