@extends('layouts.backend.app')

@section('title','Roles')

@push('css')

@endpush

@section('content')

<div class="container-fluid">
    <div class="block-header">
        @if($auth->hasPermission('admin.roles.create'))
      <a class="btn btn-primary waves effect" href="{{ route('admin.roles.create') }}">
      <li class="material-icons">add</li>
      <span>Add New Role</span>
      </a>
      @endif
    </div>

    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        All Roles
                        <span class="badge bg-blue">{{$roles->count()}}</span>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Permissions</th>
                                    <th>Updated At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                         <tbody>
                         @foreach($roles as $key=>$role)
                         <tr>
                         <td>{{$role->name}}</td>
                         <td>
                            @if($role->permissions->count() > 0 )
                            <span class="badge badge-info">{{$role->permissions->count()}}</span>
                            @else
                            <span class="badge badge-danger">No Permission Found :(</span>
                            @endif
                         </td>
                         <td>{{$role->updated_at->diffForHumans()}}</td>
                         @if($auth->hasPermission('admin.roles.edit'))
                         <td class="text-center">
                         <a href="{{route('admin.roles.edit',$role->id)}}" class="btn btn-info waves-effect">
                         <i class="material-icons">edit</i>
                         </a>
                         @endif

                         @if($auth->hasPermission('admin.roles.destroy'))
                         <button class="btn btn-danger waves effect" type="button"
                         onclick="deleterole({{$role->id}})">
                         <li class="material-icons">delete</li>
                         </button>
                         <form id="deleteform-{{$role->id}}" action="{{route('admin.roles.destroy',$role->id)}}" method="POST" style="display: none;">
                         @csrf
                        @method('DELETE')
                        @endif
                        </form>
                         </td>
                         </tr>
                         @endforeach


                         </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
</div>

@endsection

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">
    function deleterole(id)
    {
        Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
   event.preventDefault();
   document.getElementById('deleteform-'+id).submit();
  }
})
    }
    </script>


@push('js')

@endpush
