@extends('layouts.backend.app')

@section('title','Roles')

@push('css')

@endpush



@section('content')


<div class="container-fluid">

    <!-- Vertical Layout | With Floating Label -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <div>{{ isset($role) ? 'Edit ' : 'Create '}}Roles</div>
                    </h2>

                </div>
                <div class="body">
                    <form method="POST" action="{{isset($role) ? route('admin.roles.update',$role->id) : route('admin.roles.store')}}">
                        @csrf
                        @isset($role)
                        @method('PUT')
                        @endisset
                        <div class="card-body">
                        <h5 class="card-title">Manage Roles</h5>
                        <div class="form-group">
                        <label for="name">Role Name</label>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $role->name ?? old('name') }}"  autofocus>

                                   @error('name')
                                       <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                       </span>
                                   @enderror

                          </div>

                          <div class="text-center"><strong>Manage Permission For Role</strong>
                                   @error('permissions')
                                       <span class="text-danger" role="alert">
                                           <strong>{{ $message }}</strong>
                                       </span>
                                   @enderror
                                   </div>

                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                             id="select-all">
                             <label for="select-all" class="custom-control-label">Select All</label>
                            </div>
                            </div>

                         @forelse($modules->chunk(3) as $key=>$chunks)
                           <div class="form-row">
                           @foreach($chunks as $key=>$module)
                           <div class="col">
                           <h5>Module : {{ $module->name}}</h5>
                           @foreach($module->permissions as $key=> $permission)
                           <div class="mb-3 ml-4">
                           <div class="custom-control custom-checkbox mb-2">
                             <input type="checkbox" class="custom-control-input"
                             id="permission-{{$permission->id}}"
                             name="permissions[]" value="{{$permission->id}}"
                             @isset($role)
                             @foreach($role->permissions as $rpermission)
                             {{$permission->id == $rpermission->id ? 'checked' : ''}}
                             @endforeach
                             @endisset
                             >
                             <label for="permission-{{$permission->id}}" class="custom-control-label">{{$permission->name}}</label>
                           </div>
                           </div>
                           @endforeach
                           </div>
                           @endforeach
                           </div>
                         @empty
                         <div class="row">
                         <div class="col text-center">
                         <strong>No Module Found.</strong>
                         </div>
                         </div>
                         @endforelse


                        <a type="button" class="btn btn-danger m-t-15 waves-effect" href="{{ route('admin.roles.index') }}">BACK</a>
                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">
                            @isset($role)
                            Update
                            @else
                            Create
                            @endisset
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Vertical Layout | With Floating Label -->

</div>

@endsection

@push('js')
<script>
   $('#select-all').click(function(event){
       if(this.checked)
       {
           $(':checkbox').each(function(){
               this.checked = true;
           });
       }
       else
       {
           $(':checkbox').each(function(){
               this.checked = false;
           });
       }
   });
</script>
@endpush
