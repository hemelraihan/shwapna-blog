@extends('layouts.backend.app')

@section('title','category')

@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    .dropify-wrapper .dropify-message p {
        font-size: initial;
    }
</style>
@endpush

@section('content')
<div class="container-fluid">

            <!-- Vertical Layout | With Floating Label -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <div>{{ isset($category) ? 'Edit ' : 'Create '}}Categories</div>
                            </h2>

                        </div>
                        <div class="body">
                            <form action="{{ isset($category) ? route('admin.categories.update',$category->id) : route('admin.categories.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @isset($category)
                                @method('PUT')
                                @endisset
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" id="name" class="form-control @error('categoryname') is-invalid @enderror" value="{{ $category->name ?? old('name') }}" name="name">
                                        @error('categoryname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <label class="form-label">CATEGORY NAME</label>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <input type="file" class=" dropify form-control @error('image') is-invalid @enderror" data-default-file="{{ isset($category) ? Storage::disk('public')->url('categoryphoto/'.$category->image) : '' }}" name="image">
                                    @error('image')
                                    <span class="invalid-feedback" user="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>


                                <a type="button" class="btn btn-danger m-t-15 waves-effect" href="{{ route('admin.categories.index') }}">BACK</a>
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">
                                    @isset($category)
                                    Update
                                    @else
                                    Create
                                    @endisset
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Vertical Layout | With Floating Label -->

        </div>
@endsection

@push('js')

<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
  // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.dropify').dropify();

});
</script>

@endpush
