@extends('layouts.backend.app')

@section('title','Post')

@push('css')
<!-- Bootstrap Select Css -->
<link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    .dropify-wrapper .dropify-message p {
        font-size: initial;
    }
</style>
@endpush

@section('content')
<div class="container-fluid">

            <!-- Vertical Layout | With Floating Label -->
            <form action="{{ route('admin.posts.store') }}" method="POST" enctype="multipart/form-data">
             @csrf
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                ADD NEW POST

                            </h2>

                        </div>
                        <div class="body">

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" id="title" class="form-control @error('title') is-invalid @enderror" name="title">
                                        @error('title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <label class="form-label">POST TITLE</label>
                                    </div>

                                </div>

                                <div class="form-group">
                                <label for="image">Feature Image</label>
                                    <input type="file" class=" dropify form-control @error('image') is-invalid @enderror" name="image">
                                    @error('image')
                                    <span class="invalid-feedback" user="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                   @enderror
                                </div>

                                <div class="form-group">

                                    <input type="checkbox" id="publish" name="status" class="filled-in" value="1">
                                    <label for="publish">publish</label>
                                </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Categories and Tags

                            </h2>

                        </div>
                        <div class="body">

                                <div class="form-group form-float">
                                    <div class="form-line {{$errors->has('categories') ? 'focused error' : ''}}">
                                    <label for="category">Select Category</label>
                                        <select name="categories[]" class="form-control show-tick"  multiple>
                                        @foreach($categories as $category)

                                        <option value="{{$category->id}}">{{ $category->name
                                        }}</option>

                                        @endforeach
                                        </select>
                                    </div>

                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line {{$errors->has('tags') ? 'focused error' : ''}}">
                                    <label for="category">Select Tag</label>
                                        <select name="tags[]" class="form-control show-tick"  multiple>
                                        @foreach($tags as $tag)

                                        <option value="{{$tag->id}}">{{ $tag->name
                                        }}</option>

                                        @endforeach
                                        </select>
                                    </div>

                                </div>




                                <a type="button" class="btn btn-danger m-t-15 waves-effect" href="{{ route('admin.posts.index') }}">BACK</a>
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">SAVE</button>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Body

                            </h2>

                        </div>
                        <div class="body">
                            <textarea id="tinymce" name="body"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            </form>
            <!-- Vertical Layout | With Floating Label -->

        </div>
@endsection

@push('js')
 <!-- Select Plugin Js -->
 <script src="{{asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
  // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.dropify').dropify();

});
</script>
  <!-- TinyMCE -->
  <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>
  <script>
  $(function () {


    //TinyMCE
    tinymce.init({
        selector: "textarea#tinymce",
        theme: "modern",
        height: 300,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true
    });
    tinymce.suffix = ".min";
    tinyMCE.baseURL = '{{asset('assets/backend/plugins/tinymce')}}';
});</script>
@endpush
