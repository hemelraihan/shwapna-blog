@extends('layouts.backend.app')

@section('title','tag')

@section('content')
<div class="container-fluid">

            <!-- Vertical Layout | With Floating Label -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <div>{{ isset($tag) ? 'Edit ' : 'Create '}}Tags</div>
                            </h2>

                        </div>
                        <div class="body">
                            <form action="{{isset($tag) ? route('admin.tags.update',$tag->id) : route('admin.tags.store')}}" method="POST">
                                @csrf
                                @isset($tag)
                                @method('PUT')
                                @endisset
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" id="tagname" class="form-control @error('tagname') is-invalid @enderror" value="{{ $tag->name ?? old('name') }}" name="tagname">
                                        @error('tagname')
                                       <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                       </span>
                                       @enderror
                                        <label class="form-label">TAG NAME</label>
                                    </div>

                                </div>


                                <a type="button" class="btn btn-danger m-t-15 waves-effect" href="{{ route('admin.tags.index') }}">BACK</a>
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">
                                    @isset($tag)
                                    Update
                                    @else
                                    Create
                                    @endisset
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Vertical Layout | With Floating Label -->

        </div>
@endsection
