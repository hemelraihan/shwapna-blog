@extends('layouts.frontend.app')

@section('title','Post Details')

@push('css')

<link href="{{asset('assets/frontend/css/home/styles.css')}}" rel="stylesheet">

<link href="{{asset('assets/frontend/css/home/responsive.css')}}" rel="stylesheet">
@endpush

@section('content')

  <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
  <main id="mainContent" class="main-content">
    <!-- Page Container -->
    <div class="page-container ptb-60">
        <div class="container">
            <div class="row row-rl-10 row-tb-20">
                <div class="page-content col-xs-12 col-sm-7 col-md-8">
                    <div class="row row-tb-20">
                        @foreach ($posts as $post)
                        @if ($post->is_approved == true)
                        <div class="col-xs-12">
                            <div class="deal-deatails panel">
                                <div class="deal-slider">
                                    <div id="product_slider" class="flexslider">


                                        <ul class="slides">
                                            <li>
                                                <img alt="" src="{{Storage::disk('public')->url('postphoto/'.$post->image)}}">
                                            </li>

                                        </ul>
                                    </div>

                                </div>
                                <div class="deal-body p-20">
                                    <h3 class="mb-10">{{$post->title}}</h3>

                                    <h6 class=" mb-15">
                                        <li><i class="icon fa fa-user"></i> By : {{$post->admin->name}}</li>
                                        <li><i class="fa fa-list-alt"></i> Category :
                                        @foreach($post->categories as $category)
                                        {{$category->name}}
                                        @endforeach
                                        </li>

                                        <li><i class="icon fa fa-tag"></i> Tag :
                                            @foreach($post->tags as $tag)
                                            {{$tag->name}},
                                            @endforeach
                                            </li>
                                    </h6>
                                    <p class="mb-15">{!! html_entity_decode($post->body) !!}</p>
                                </div>
                            </div>
                        </div>
                        @else
                        <h1 style="color: red">There are no post for this id...!!!!</h1>
                        @endif
                        @endforeach


                         @foreach ($posts as $post)
                         @if ($post->is_approved == true)

                        <div class="col-xs-12">
                            <div class="posted-review panel p-30">
                                <h3 class="h-title">{{$post->comments->count() }} Comments</h3>
                                @foreach ($post->comments as $comment)
                                <div class="review-single pt-30">
                                    <div class="media">
                                        <div class="media-left">
                                            <img class="media-object mr-10 radius-4" src="{{Storage::disk('public')->url('userphoto/'.$comment->user->image)}}" width="90" alt="">
                                        </div>
                                        <div class="media-body">
                                            <div class="review-wrapper clearfix">
                                                <ul class="list-inline">
                                                    <li>
                                                        <span class="review-holder-name h5">{{$comment->user->name}}</span>
                                                    </li>
                                                    <li>
                                                        <div class="rating">
                                                            <span class="rating-stars" data-rating="5">
                                                        <i class="fa fa-star-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                    </span>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <p class="review-date mb-5">{{$comment->created_at->diffForHumans()}}</p>
                                                <p class="copy">{{$comment->comment}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        @else
                        <h1 style="color: red">There are no post for this id...!!!!</h1>
                        @endif
                        @endforeach

                        @if ($post->is_approved == true)
                        <div class="col-xs-12">
                            <div class="post-review panel p-20">
                                <h3 class="h-title">Post Review</h3>
                                @guest

                                <p>For Post a new comment. You need to login first, <a href="{{route('login')}}">Login</a></p>

                                @else
                                @foreach ($posts as $post)
                               <form class="horizontal-form pt-30" method="post" action="{{route('comment.store',$post->id)}}">
                                @csrf
                                    <div class="row row-v-10">

                                        <div class="col-xs-12">
                                            <ul class="select-rate list-inline ptb-20">
                                                <li><span>Your Rating : </span>
                                                </li>
                                                <li>
                                                    <span class="rating" role="button">
                                            <i class="fa fa-star"></i>
                                        </span>
                                                </li>
                                                <li>
                                                    <span class="rating" role="button">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </span>
                                                </li>
                                                <li>
                                                    <span class="rating" role="button">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </span>
                                                </li>
                                                <li>
                                                    <span class="rating" role="button">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </span>
                                                </li>
                                                <li>
                                                    <span class="rating" role="button">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-12">
                                            <textarea name="comment" class="form-control" placeholder="Post Your Comment" rows="6"></textarea>
                                        </div>
                                        <div class="col-xs-12 text-right">
                                            <button type="submit" class="btn mt-20">Submit Comment</button>
                                        </div>
                                    </div>
                                </form>
                                @endforeach
                                @endguest
                            </div>
                        </div>
                        @else
                        <h1 style="color: red">There are no post for this id...!!!!</h1>
                        @endif
                    </div>
                </div>
                <div class="page-sidebar col-md-4 col-sm-5 col-xs-12">
                    <!-- Blog Sidebar -->

                    <div class="col-xs-12">
                        <!-- Best Rated Deals -->
                        <div class="widget best-rated-deals panel pt-20 prl-20">
                            <h3 class="widget-title h-title">Recent Posts</h3>
                            <div class="widget-body ptb-30">

                            @foreach ($recentposts as $recentpost)

                                <div class="media">
                                    <div class="media-left">

                                        <a href="{{route('home.postdetails',$recentpost->id)}}">


                                            <img class="media-object" src="{{Storage::disk('public')->url('postphoto/'.$recentpost->image)}}" alt="Thumb" width="80">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="mb-5">
                                                    <a href="{{route('home.postdetails',$recentpost->id)}}">{{$recentpost->title}}</a>
                                                </h6>

                                        <h4 class="price font-16"><i class="icon fa fa-user"></i> By : {{$recentpost->admin->name}}</h4>
                                        <p><i class="icon fa fa-calendar"> : </i> {{$recentpost->created_at->format('F-d-y')}}</p>
                                    </div>
                                </div>

                                @endforeach

                           </div>
                        </div>

                   </div>

            </div>

            <div class="page-sidebar col-md-4 col-sm-5 col-xs-12">
                <!-- Blog Sidebar -->

                <div class="col-xs-12">
                    <!-- Best Rated Deals -->
                    <div class="widget best-rated-deals panel pt-20 prl-20">
                        <h3 class="widget-title h-title">Related Posts</h3>
                        <div class="widget-body ptb-30">

                            
                        
                        @foreach ($categories as $category)
                        
                        
                        @foreach ($category->posts as $post)
                        @if ($post->is_approved == true)
                        @foreach ($posts as $allpost)
                        
                        @if($post->id != $allpost->id)

                               <div class="media">
                                <div class="media-left">

                                    <a href="{{route('home.postdetails',$post->id)}}">


                                        <img class="media-object" src="{{Storage::disk('public')->url('postphoto/'.$post->image)}}" alt="Thumb" width="80">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h6 class="mb-5">
                                                <a href="{{route('home.postdetails',$post->id)}}">{{$post->title}}</a>
                                            </h6>

                                    <h4 class="price font-16"><i class="icon fa fa-user"></i> By : {{$post->admin->name}}</h4>
                                    <p><i class="icon fa fa-calendar"> : </i> {{$post->created_at->format('F-d-y')}}</p>
                                </div>
                            </div>
                            @endif
                            
                            @endforeach
                            @endif
                            @endforeach
                            @endforeach
                            
                            

                       </div>
                    </div>

               </div>

        </div>

        </div>
    </div>
    <!-- End Page Container -->


</main>
<!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->


@endsection

@push('js')



@endpush


