@extends('layouts.frontend.app')

@section('title')

{{$query}}

@endsection

@push('css')

<link href="{{asset('assets/frontend/css/home/styles.css')}}" rel="stylesheet">

<link href="{{asset('assets/frontend/css/home/responsive.css')}}" rel="stylesheet">
@endpush

@section('content')

<!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
<main id="mainContent" class="main-content">
    <div class="page-container ptb-60">
        <div class="container">
            <div class="row row-rl-10 row-tb-20">
                <div class="page-content col-xs-12 col-md-8">


                    <section class="section deals-area">

                        <!-- Page Control -->
                        <header class="page-control panel ptb-15 prl-20 pos-r mb-30">

                            <!-- List Control View -->
                            <ul class="list-control-view list-inline">
                                <li><a href="deals_list.html"><i class="fa fa-bars"></i></a>
                                </li>
                                <li><a href="deals_grid.html"><i class="fa fa-th"></i></a>
                                </li>
                            </ul>
                            <!-- End List Control View -->

                            <div class="right-10 pos-tb-center">
                                <select class="form-control input-sm">
                                    <option>SORT BY</option>
                                    <option>Newest items</option>
                                    <option>Best sellers</option>
                                    <option>Best rated</option>
                                    <option>Price: low to high</option>
                                    <option>Price: high to low</option>
                                </select>
                            </div>
                        </header>
                        <!-- End Page Control -->
                        <div class="row row-masnory row-tb-20">
                            @foreach($posts as $post)

                            <div class="col-sm-6">
                                <div class="deal-single panel">
                                    <figure class="deal-thumbnail embed-responsive embed-responsive-16by9" data-bg-img="{{Storage::disk('public')->url('postphoto/'.$post->image)}}">
                                        <div class="label-discount left-20 top-15">-50%</div>
                                        <ul class="deal-actions top-15 right-20">
                                            <li class="like-deal">
                                                <span>
                                        <i class="fa fa-heart"></i>
                                    </span>
                                            </li>
                                            <li class="share-btn">
                                                <div class="share-tooltip fade">
                                                    <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                                                    <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                                                    <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                                                    <a target="_blank" href="#"><i class="fa fa-pinterest"></i></a>
                                                </div>
                                                <span><i class="fa fa-share-alt"></i></span>
                                            </li>
                                            <li>
                                                <span>
                                        <i class="fa fa-camera"></i>
                                    </span>
                                            </li>
                                        </ul>
                                        <div class="time-left bottom-15 right-20 font-md-14">
                                            <span>
                                    <i class="ico fa fa-clock-o mr-10"></i>
                                    <span class="t-uppercase" data-countdown="2019/09/01 01:30:00"></span>
                                </span>
                                        </div>
                                        <div class="deal-store-logo">
                                            <img src="assets/images/brands/brand_01.jpg" alt="">
                                        </div>
                                    </figure>
                                    <div class="bg-white pt-20 pl-20 pr-15">
                                        <div class="pr-md-10">
                                            <div class="rating mb-10">
                                                <span class="rating-stars rate-allow" data-rating="5">
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                                <span class="rating-reviews">
                                        ( <span class="rating-count">241</span> rates )
                                                </span>
                                            </div>
                                            <h3 class="deal-title mb-10">
                                    <a href="{{route('home.postdetails',$post->id)}}">{{$post->title}}</a>
                                </h3>
                                            <ul class="deal-meta list-inline mb-10 color-mid">
                                                <li><i class="icon fa fa-user"></i> By : {{$post->user->name}}</li>

                                            </ul>

                                 <p class="text-muted mb-20">{{Str::limit( html_entity_decode($post->body,'100') )}}</p>


                                        </div>

                                    </div>
                                </div>
                            </div>

                             @endforeach
                        </div>

                        <!-- Page Pagination -->

                        <!-- End Page Pagination -->

                    </section>

                </div>
                <div class="page-sidebar col-md-4 col-xs-12">

                    <!-- Blog Sidebar -->
                    <aside class="sidebar blog-sidebar">
                        <div class="row row-tb-10">
                            <div class="col-xs-12">
                                <!-- Latest Deals Widegt -->

                                <!-- End Latest Deals Widegt -->
                            </div>
                            <div class="col-xs-12">
                                <!-- Best Rated Deals -->
                                <div class="widget best-rated-deals panel pt-20 prl-20">
                                    <h3 class="widget-title h-title">Recent Posts</h3>
                                    <div class="widget-body ptb-30">

                                    @foreach ($recentposts as $recentpost)

                                        <div class="media">
                                            <div class="media-left">

                                                <a href="">
                                                    <img class="media-object" src="{{Storage::disk('public')->url('postphoto/'.$recentpost->image)}}" alt="Thumb" width="80">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="mb-5">
                                                        <a href="#">{{$recentpost->title}}</a>
                                                    </h6>

                                                <h4 class="price font-16"><i class="icon fa fa-user"></i> By : {{$recentpost->user->name}}</h4>
                                                <p><i class="icon fa fa-calendar"> : </i> {{$recentpost->created_at->format('F-d-y')}}</p>
                                            </div>
                                        </div>

                                        @endforeach

                                   </div>
                                </div>
                                <!-- Best Rated Deals -->


                            </div>
                            <div class="col-xs-12">
                                <!-- Subscribe Widget -->
                                <div class="widget subscribe-widget panel pt-20 prl-20">
                                    <h3 class="widget-title h-title">Subscribe to mail</h3>
                                    <div class="widget-content ptb-30">

                                        <p class="color-mid mb-20">Get our Daily email newsletter with Special Services, Updates, Offers and more!</p>
                                        <form method="post" action="#">
                                            <div class="input-group">
                                                <input type="email" class="form-control" placeholder="Your Email Address" required="required">
                                                <span class="input-group-btn">
            <button class="btn" type="submit">Sign Up</button>
        </span>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                                <!-- End Subscribe Widget -->
                            </div>

                            </div>


                        </div>
                    </aside>
                    <!-- End Blog Sidebar -->
                </div>
            </div>
        </div>
    </div>


</main>
<!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->

@endsection

@push('js')



@endpush


