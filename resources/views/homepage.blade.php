@extends('layouts.frontend.app')

@section('title','Home')

@push('css')

<link href="{{asset('assets/frontend/css/home/styles.css')}}" rel="stylesheet">

<link href="{{asset('assets/frontend/css/home/responsive.css')}}" rel="stylesheet">

<style>
    .favorite_posts{
        color: blue;
    }
</style>

@endpush

@section('content')

<main id="mainContent" class="main-content">
            <div class="page-container ptb-10">
                <div class="container">




                    <section class="section latest-coupons-area ptb-30">


                        <div class="latest-coupons-slider owl-slider" data-autoplay-hover-pause="true" data-loop="true" data-autoplay="true" data-smart-speed="1000" data-autoplay-timeout="10000" data-margin="30" data-nav-speed="false" data-items="1" data-xxs-items="1" data-xs-items="2" data-sm-items="2" data-md-items="3" data-lg-items="4">
                                                       </div>
                             </section>


                             <div class="section deals-header-area ptb-30">
                                <div class="row row-tb-20">

                                    <div class="col-xs-12 col-md-4 col-lg-3">
                                        <aside>
                                            <ul class="nav-coupon-category panel">
                                                @foreach ($categories as $category)

                                                <li><a href="{{route('home.post',$category->id)}}"><i class="fa fa-cutlery"></i>{{$category->name}}<span>{{$category->posts->count()}}</span></a>
                                                </li>
                                                @endforeach
                                                <li class="all-cat">
                                                    <a class="font-14" href="#">All Categories</a>
                                                </li>
                                            </ul>
                                        </aside>
                                    </div>

                                    <div class="col-xs-12 col-md-8 col-lg-9">
                                        <div class="header-deals-slider flexslider" id="header-deals-slider">
                                            <ul class="slides">
                                                <li>
                                                    <div class="deal-single panel item">
                                                        <figure class="deal-thumbnail embed-responsive embed-responsive-16by9" data-bg-img="assets/frontend/images/slider1.jpg">
                                                            <div class="label-discount top-10 right-10">-50%</div>
                                                            <ul class="deal-actions top-10 left-10">
                                                                <li class="like-deal">
                                                                    <span>
                                                                        <i class="fa fa-heart"></i>
                                                                    </span>
                                                                </li>
                                                                <li class="share-btn">
                                                                    <div class="share-tooltip fade">
                                                                        <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                                                                        <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                                                                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                                                                        <a target="_blank" href="#"><i class="fa fa-pinterest"></i></a>
                                                                    </div>
                                                                    <span><i class="fa fa-share-alt"></i></span>
                                                                </li>
                                                                <li>
                                                                    <span>
                                                                        <i class="fa fa-camera"></i>
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                            <div class="deal-about p-20 pos-a bottom-0 left-0">
                                                                <div class="rating mb-10">
                                                                    <span class="rating-stars" data-rating="5">
                                                                        <i class="fa fa-star-o star-active"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </span>
                                                                    <span class="rating-reviews color-light">
                                                                        ( <span class="rating-count">241</span> Reviews )
                                                                    </span>
                                                                </div>
                                                                <h3 class="deal-title mb-10 ">
                                                                    <a href="deal_single.html" class="color-light color-h-lighter">The Crash Bad Instant Folding Twin Bed</a>
                                                                </h3>
                                                            </div>
                                                        </figure>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="deal-single panel item">
                                                        <figure class="deal-thumbnail embed-responsive embed-responsive-16by9"  data-bg-img="assets/frontend/images/slider2.jpg">
                                                            <div class="label-discount top-10 right-10">-30%</div>
                                                            <ul class="deal-actions top-10 left-10">
                                                                <li class="like-deal">
                                                                    <span>
                                                                        <i class="fa fa-heart"></i>
                                                                    </span>
                                                                </li>
                                                                <li class="share-btn">
                                                                    <div class="share-tooltip fade">
                                                                        <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                                                                        <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                                                                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                                                                        <a target="_blank" href="#"><i class="fa fa-pinterest"></i></a>
                                                                    </div>
                                                                    <span><i class="fa fa-share-alt"></i></span>
                                                                </li>
                                                                <li>
                                                                    <span>
                                                                        <i class="fa fa-camera"></i>
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                            <div class="deal-about p-20 pos-a bottom-0 left-0">
                                                                <div class="rating mb-10">
                                                                    <span class="rating-stars" data-rating="5">
                                                                        <i class="fa fa-star-o star-active"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </span>
                                                                    <span class="rating-reviews color-light">
                                                                    ( <span class="rating-count">132</span> Reviews )
                                                                    </span>
                                                                </div>
                                                                <h3 class="deal-title mb-10 ">
                                                                    <a href="deal_single.html" class="color-light color-h-lighter">Western Digital USB 3.0 Hard Drives</a>
                                                                </h3>
                                                            </div>
                                                        </figure>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="deal-single panel item">
                                                        <figure class="deal-thumbnail embed-responsive embed-responsive-16by9"  data-bg-img="assets/frontend/images/slider3.jpg">
                                                            <div class="label-discount top-10 right-10">-30%</div>
                                                            <ul class="deal-actions top-10 left-10">
                                                                <li class="like-deal">
                                                                    <span>
                                                                        <i class="fa fa-heart"></i>
                                                                    </span>
                                                                </li>
                                                                <li class="share-btn">
                                                                    <div class="share-tooltip fade">
                                                                        <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                                                                        <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                                                                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                                                                        <a target="_blank" href="#"><i class="fa fa-pinterest"></i></a>
                                                                    </div>
                                                                    <span><i class="fa fa-share-alt"></i></span>
                                                                </li>
                                                                <li>
                                                                    <span>
                                                                        <i class="fa fa-camera"></i>
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                            <div class="deal-about p-20 pos-a bottom-0 left-0">
                                                                <div class="rating mb-10">
                                                                    <span class="rating-stars" data-rating="5">
                                                                        <i class="fa fa-star-o star-active"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </span>
                                                                    <span class="rating-reviews color-light">
                                                                    ( <span class="rating-count">160</span> Reviews )
                                                                    </span>
                                                                </div>
                                                                <h3 class="deal-title mb-10 ">
                                                                    <a href="deal_single.html" class="color-light color-h-lighter">Hampton Bay LED Light Ceiling Exhaust Fan</a>
                                                                </h3>
                                                            </div>
                                                        </figure>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>



                                </div>
                            </div>






                    <section class="section latest-news-area blog-area blog-grid blog-3-col ptb-30">
                        <header class="panel ptb-15 prl-20 pos-r mb-30">
                            <h3 class="section-title font-18">Latest Posts</h3>
                            <a href="blog_classic_right_sidebar.html" class="btn btn-o btn-xs pos-a right-10 pos-tb-center">Visit Blog</a>
                        </header>

                        <div class="row row-masnory row-tb-20">

                            <!-- Blog Post -->

                            @foreach ($posts as $post)

                            @if ($post->is_approved == true)

                            <div class="blog-post col-xs-12 col-sm-6 col-md-4 ">
                                <article class="entry panel">
                                    <figure class="entry-media post-thumbnail embed-responsive embed-responsive-16by9" data-bg-img="{{Storage::disk('public')->url('postphoto/'.$post->image)}}">

                                        <div class="entry-date">
                                            <h4>{{$post->created_at->format('d')}}</h4>
                                            <h6>{{$post->created_at->format('F')}}</h6>
                                        </div>
                                    </figure>
                                    <div class="entry-wrapper pt-20 pb-10 prl-20">
                                        <header class="entry-header">
                                            <h4 class="entry-title mb-10 mb-md-15 font-xs-16 font-sm-18 font-md-16 font-lg-16">

							<a href="{{route('home.postdetails',$post->id)}}">{{$post->title}}</a>

						</h4>
                                            <div class="entry-meta mb-10">
                                                <ul class="tag-info list-inline">
                                                    <li><i class="icon fa fa-user"></i> By : {{$post->admin->name}}</li>
                                                    <li><i class="fa fa-list-alt"></i> Category :
                                                    @foreach($post->categories as $category)
                                                    {{$category->name}}
                                                    @endforeach
                                                    </li>

                                                    <li>
                                                        <i class="icon fa fa-tag"></i> Tag :
                                                        @foreach($post->tags as $tag)
                                                        {{$tag->name}},
                                                        @endforeach
                                                    </li>
                                                    <li>

                                                        @guest

                                                        <a href="{{route('login')}}"  > <i class="icon fa fa-heart"></i>
                                                            {{$post->favorite_to_users->count()}}</a>

                                                        @else
                                                      <button type="button" onclick="deletepost$post({{$post->id}})" class="{{!Auth::user()->favorite_posts->where('pivot.post_id',$post->id)->count() == 0 ? 'favorite_posts' : ''}}" > <i class="icon fa fa-heart"></i>
                                                            {{$post->favorite_to_users->count()}}</button>
                                                            <form action="{{route('post.favorite',$post->id)}}" method="POST" id="favorite-form-{{$post->id}}" style="display:none;">
                                                                @csrf
                                                            </form>

                                                            @endguest

                                                    </li>
                                                    <li>
                                                        <i class="icon fa fa-eye"></i>
                                                        {{$post->view_count}}
                                                    </li>

                                                    <li><i class="icon fa fa-comments"></i>{{$post->comments->count()}} Comments </li>

                                                </ul>
                                            </div>
                                        </header>
                                        <div class="entry-content">
                                            <p class="entry-summary">{!! html_entity_decode($post->body) !!}</p>
                                        </div>
                                        <footer class="entry-footer text-right">
                                            <a href="{{route('home.postdetails',$post->id)}}" class="more-link btn btn-link">Continue reading <i class="icon fa fa-long-arrow-right"></i></a>
                                        </footer>
                                    </div>
                                </article>
                            </div>



                            @endif
                            @endforeach
                            <!-- End Blog Post -->

                     </div>

                      <!-- Page Pagination -->
                      <div class="page-pagination text-center mt-30 p-10 panel">
                        <nav>
                            <!-- Page Pagination -->

                               {{$posts->links()}}

                            <!-- End Page Pagination -->
                        </nav>
                    </div>
                    <!-- End Page Pagination -->

                    </section>


                    <section class="section subscribe-area ptb-40 t-center">
                        <div class="newsletter-form">
                            <h4 class="mb-20"><i class="fa fa-envelope-o color-green mr-10"></i>Sign up for our weekly email newsletter</h4>
                            <p class="mb-20 color-mid">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi animi magni accusantium architecto possimus.</p>
                            <form method="POST" action="{{route('subscriber.store')}}">
                                @csrf
                                <div class="input-group mb-10">
                                    <input name="email" class="form-control bg-white" type="email" required="required" placeholder="Enter your email">
                                    <span class="input-group-btn">
                                    <button class="btn" type="submit">Subscribe</button>
                                    </span>
                                </div>
                                </form>
                            <p class="color-muted"><small>We’ll never share your email address with a third-party.</small> </p>
                        </div>
                    </section>
                </div>
            </div>




        </main>
@endsection

<script type="text/javascript">
    function deletepost$post(id)
    {
        document.getElementById('favorite-form-'+id).submit();
    }
</script>


@push('js')



@endpush


