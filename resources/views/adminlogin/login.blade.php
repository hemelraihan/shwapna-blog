@extends('layouts.frontend.app')

@section('title','Login')

@push('css')
<link href="{{asset('assets/frontend/css/auth/styles.css')}}" rel="stylesheet">

<link href="{{asset('assets/frontend/css/auth/responsive.css')}}" rel="stylesheet">
@endpush


@section('content')
 <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
 <main id="mainContent" class="main-content">
    <div class="page-container ptb-60">
        <div class="container">
            <section class="sign-area panel p-40">
                <h3 class="sign-title">Admin Sign In <small>Or <a href="{{route('register')}}" class="color-green">Sign Up</a></small></h3>
                <div class="row row-rl-0">
                    <div class="col-sm-6 col-md-7 col-left">
                        <form class="p-40" action="{{ route('admin.login') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label class="sr-only">Email</label>
                                <input type="email" name="email" value="{{ old('email') }}" class="form-control input-lg @error('email') is-invalid @enderror" placeholder="Email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="sr-only">Password</label>
                                <input type="password"  class="form-control input-lg @error('password') is-invalid @enderror" name="password" placeholder="Password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            
                            <div class="custom-checkbox mb-20">
                                <input type="checkbox" id="remember_account" {{ old('remember') ? 'checked' : '' }}>
                                <label class="color-mid" for="remember_account">Keep me signed in on this computer.</label>
                            </div>
                            <button type="submit" class="btn btn-block btn-lg">Sign In</button>
                        </form>
                    </div>
                   
                </div>
            </section>
        </div>
    </div>


</main>
<!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection


@push('js')

@endpush
