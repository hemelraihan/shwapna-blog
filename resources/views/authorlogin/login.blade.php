@extends('layouts.frontend.app')

@section('title','Login')

@push('css')
<link href="{{asset('assets/frontend/css/auth/styles.css')}}" rel="stylesheet">

<link href="{{asset('assets/frontend/css/auth/responsive.css')}}" rel="stylesheet">
@endpush


@section('content')
 <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
 <main id="mainContent" class="main-content">
    <div class="page-container ptb-60">
        <div class="container">
            <section class="sign-area panel p-40">
                <h3 class="sign-title">Sign In <small>Or <a href="{{route('register')}}" class="color-green">Sign Up</a></small></h3>
                <div class="row row-rl-0">
                    <div class="col-sm-6 col-md-7 col-left">
                        <form class="p-40" action="{{ route('login') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label class="sr-only">Email</label>
                                <input type="email" name="email" value="{{ old('email') }}" class="form-control input-lg @error('email') is-invalid @enderror" placeholder="Email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="sr-only">Password</label>
                                <input type="password"  class="form-control input-lg @error('password') is-invalid @enderror" name="password" placeholder="Password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                @if (Route::has('password.request'))
                                <a href="{{ route('password.request') }}" class="forgot-pass-link color-green">Forget Your Password ?</a>
                                @endif
                            </div>
                            <div class="custom-checkbox mb-20">
                                <input type="checkbox" id="remember_account" {{ old('remember') ? 'checked' : '' }}>
                                <label class="color-mid" for="remember_account">Keep me signed in on this computer.</label>
                            </div>
                            <button type="submit" class="btn btn-block btn-lg">Sign In</button>
                        </form>
                        <span class="or">Or</span>
                    </div>
                    <div class="col-sm-6 col-md-5 col-right">
                        <div class="social-login p-40">
                            <div class="mb-20">
                                <button class="btn btn-lg btn-block btn-social btn-facebook"><i class="fa fa-facebook-square"></i>Sign In with Facebook</button>
                            </div>
                            <div class="mb-20">
                                <a href="{{route('login.provider','github')}}" class="btn btn-lg btn-block btn-social btn-github"><i class="fa fa-github"></i>Sign In with GitHub</a>
                            </div>
                            <div class="mb-20">
                                <button class="btn btn-lg btn-block btn-social btn-google-plus"><i class="fa fa-google-plus"></i>Sign In with Google</button>
                            </div>
                            <div class="custom-checkbox mb-20">
                                <input type="checkbox" id="remember_social" checked>
                                <label class="color-mid" for="remember_social">Keep me signed in on this computer.</label>
                            </div>
                            <div class="text-center color-mid">
                                Need an Account ? <a href="{{ route('register') }}" class="color-green">Create Account</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>


</main>
<!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection


@push('js')

@endpush
